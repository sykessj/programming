package sports;

import sports.Console;//Imports the console class

public class Booking 
{
	
	String name;//Creates the variable name which is s string
	String sport;//Creates the variable sport which is a string
	int courtno;//Creates the variable courtno which is an integer
	String Slot;//creates the variable slot which is a string
	String Day;//Creates the variable Day which is a string
	
	//defauls constructer
	public Booking()//Creates the function Booking
	{
		set("???", "???", 0, "???","???");//Defines the set
	}
	
	//construct with parameters
	public Booking(String name, String sport, int courtno, String Slot, String Day)//Defines the different variables within booking set
	{
		set(name, sport, courtno, Slot, Day);//Links those variables with parameters
	}
	public void set(String name, String sport, int courtno, String Slot, String Day)// defines the set with parameters
	{//Links the variables in this set to the variables created earlier
		this.name = name;
		this.sport = sport;
		this.courtno = courtno;
		this.Slot = Slot;
		this.Day = Day;
	}
	public void print() //Initialises the print function
	{
		System.out.println("Name of the booking => " + name);//Prints the name of the booking
		System.out.println("The sport you have selected is: " + sport);//Prints the sport chosen
		System.out.println("You are on court => " + courtno);//Shows the court assigned
		System.out.println("Your time slot is => " + Slot);//Shows the chosen time slot
		System.out.println("The day of your booking is => " + Day);//Show the chosen day
	}
	public String getName()//defines the getname function
	{
		return name;//Returns the name variable
	}
	
	public void ask()//creates the ask function
	{
		name = Console.askString("Enter name => ");//Asks for the name of the booking
		sport = Console.askString("Enter sport => ");//Asks for the sport
		courtno = Console.askInt("Enter court number => ");//Asks for the court number
		Day = Console.askString("Enter Day => ");//Asks for the day to be played on
		Slot = Console.askString("Enter time => ");//Asks the user for the time
		
	}
	public String toString()//Defines the toString function
	{
		return "Name => " + name + "      " + "Sport => " + sport + "      " + "Court number => " + courtno + "      " + "Time Slot => " + Slot + "      " + "Day => " + Day;
		//This prints the bookings in a string.
	}
	
	

}
