package sports;
import java.util.ArrayList;//Imports the java function array list



public class records {
	ArrayList<Booking>bookings;//Declares the array list of the bookings
	ArrayList<String>names;////Declares the array list names
	
	public records()//Creates the function records
	{
	bookings = new ArrayList<Booking>();//Initialises the new array list bookings
	names = new ArrayList<String>();//Initialises the new array list names
	}
	

	public void addBooking(Booking name)//Creates the add booking function
	{
		bookings.add(name);//uses the add function to add a the specified booking to the booking array
		names.add(name.getName());//Adds the new booking in the names array
	}
	
	public void removeBooking(Booking name)//Creates the remove booking function
	{
		bookings.remove(name);//Removes the booking from the booking array list
		names.remove(name.getName());//Removes the booking from the names array list
	}
	
	public void print()//Creates another print function
		{
			for (Booking temp: bookings)//starts a for loop that creates a temporary variable in the bookings array
			{
				System.out.println(temp);//Prints the temp variable
			}
		}
		
	
	public Booking find(String name)//creates the find function
	{
		int index = names.indexOf(name);//creates the index variable which finds the index of the names array of the given name
			if (index == -1)//starts an if function to check if the index in -1
			{
				return null;//If so it will return null
			}
			else
			{
				return bookings.get(index);//Otherwise returns the element found in the array
			}
	}
		
}
