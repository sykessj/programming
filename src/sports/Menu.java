package sports;
import java.util.Scanner;//Imports the scanner for the user input


public class Menu {
	public static void main(String[] args)
	{
		Scanner input = new Scanner (System.in);//initialising the scanner
		
		records Bookings = new records();//uses the records function from on the Bookings array
		boolean quit = false;//makes the quit variable false
	
		while (quit == false) //a while loop that loops when the quit variable is false
		{
			System.out.println("Please select an choice");//Instructs the user
			System.out.println("1.Book a Slot   2.Find a Booking   3.Print Current bookings   4.Cancel a Booking   5.Quit");//gives the user options
			System.out.print("=> ");//prompts the user to enter an option
			int choice = input.nextInt();//saves the user input in the variable choice
			
			if (choice == 1)//starts an if function,if the choice variable is equal to 1
			{
				Booking one = new Booking(); //initialises a booking variable called one
				one.ask();//Asks for the options such as the name of the booking
				Bookings.addBooking(one);//Uses the add booking function
				System.out.println("Addition Completed");//Tells the user that the process is completed
				System.out.println();//Prints a line to make the program look better
			}
			else if (choice == 2)//If the user inputs is 2 it will search for the record
			{
				String name = Console.askString("Enter Name: ");//Takes the users input for the name of the booking
				System.out.println(Bookings.find(name));//prints the name class with the corresponding name using the find function
				System.out.println();//Prints a line
			}
			else if (choice == 3)//If the user inputs 3 the system will print the current bookings
			{
				Bookings.print();//Prints the current bookings
				System.out.println();//Prints the current booking 
			}
			else if (choice == 4)//if the user input is 4, the program will remove a booking
			{
				String name = Console.askString("Enter name of booking: ");//This asks the user for the booking to be removed
				Booking one = Bookings.find(name);//Creates a variable called one using the booking function to find a booking with corresponding name
				Bookings.removeBooking(one);//Uses the remove booking function to remove the one variable
				System.out.println("Booking Cancelled");//Tells the user the booking has been removed
				System.out.println();//Prints a blank line
				
			}
			else if (choice == 5)//If the user input is 5 they system will quit
			{
				quit = true;//Changes the quit variable to true so the loop will stop
				System.out.println("Quitting Program");//Tells the user that the program is not quitting
			}
			else
			{
				System.out.println("That was not a valid selection");//Otherwise it will prints that it was not a valid selection and returns to the menu
				System.out.println();//prints a blank line
				
			}
		}
	}

}
