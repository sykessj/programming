package sports;

import sports.Booking;

public class Test_Booking {
	public static void main(String[] args)
	{
		Booking one = new Booking();
		one.print();
		System.out.println();
		
		Booking two = new Booking("Fred", 1, "16:00", "Wednesday");
		two.print();
		System.out.println();
		
		Booking three = new Booking();
		three.ask();
		three.print();
		System.out.println();
		
		System.out.print(one);
		System.out.print(two);
		System.out.print(three);
		
	}

}
